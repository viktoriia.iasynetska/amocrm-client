<?php

require_once "vendor/autoload.php";

use core\Request;
use core\LangManager;
use controllers\FormController;

//auto-load Classes
spl_autoload_register(function ($classname)
{
    require_once __DIR__ . DIRECTORY_SEPARATOR .'app' .DIRECTORY_SEPARATOR. str_replace('\\', DIRECTORY_SEPARATOR, $classname) . '.php';
});
session_start();

$request = new Request($_GET, $_POST, $_COOKIE, $_FILES);
$langManager = new LangManager($request);
$controller = new FormController($request, $langManager);

switch($request->getRequestMethod())
{
    case $request::METHOD_POST:
        $controller->postIndexRequest();
        break;
    case $request::METHOD_GET:
        $controller->getIndexRequest();
        break;
}

$controller->render();