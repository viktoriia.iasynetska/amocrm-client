<?php
namespace controllers;

use clients\ApiLeadClient;
use clients\ApiTaskClient;
use clients\ApiUserClient;
use entities\ContactForm;
use entities\converters\ContactConverter;
use entities\converters\LeadConverter;
use entities\converters\TaskConverter;
use entities\converters\UserConverter;
use Exception;
use oauth2\AmoCrmOAuth2Client;
use clients\ApiContactClient;
use services\ContactFormService;
use services\email\EmailService;

class FormController extends AbstractHtmlController
{
    public function getIndexRequest()
    {
        $this->title = 'amoCRM-Client';
        $this->jsFile = 'js/common.js';

        $this->content = $this->build(
            (dirname(__DIR__, 1)). '/views/form.html.php',
            [
                'name' => $this->langManager->getLangParams()['name'],
                'phone' => $this->langManager->getLangParams()['phone'],
                'email' => $this->langManager->getLangParams()['email'],
                'required_fields' => $this->langManager->getLangParams()['required_fields'],
                'send' => $this->langManager->getLangParams()['send']
            ]
        );
    }

    public function postIndexRequest()
    {
        $amoCrmOAuth2Client = new AmoCrmOAuth2Client();
        $apiContactClient = new ApiContactClient($amoCrmOAuth2Client, new ContactConverter());
        $apiLeadClient  = new ApiLeadClient($amoCrmOAuth2Client, new LeadConverter());
        $apiTaskClient = new ApiTaskClient($amoCrmOAuth2Client, new TaskConverter());
        $apiUserClient = new ApiUserClient($amoCrmOAuth2Client, new UserConverter());

        $contactFormService = new ContactFormService($apiContactClient, $apiLeadClient, $apiTaskClient, $apiUserClient);

        $name = $this->request->getPostParam('name');
        $phone = $this->request->getPostParam('phone');
        $email = $this->request->getPostParam('email');

        $contactForm = new ContactForm($phone, $email, $name);

        try {
            $contactFormService->handleContactForm($contactForm);
            $this->content = $this->langManager->getLangParams()['successful_result'];

            $mailService = new EmailService();
            $mailService->sendEmail($contactForm);
        } catch (Exception $e){
            $this->content = 'Error: ' . $e->getMessage();
        }
    }


}
