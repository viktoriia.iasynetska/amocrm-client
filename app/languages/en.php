<?php
return array (
    "name" => "Name",
    "phone" => "Phone",
    "email" => "E-mail",
    "required_fields" => "required fields",
    "send" => "Send",
    "successful_result" => "Thank you for your application!"
);
