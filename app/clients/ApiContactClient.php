<?php
namespace clients;

use oauth2\OAuth2ClientInterface;
use entities\converters\ContactConverter;
use entities\Contact;
use \Exception;


class ApiContactClient extends AbstractApiClient implements ContactClientInterface
{
    private $link = "https://yasinetv.amocrm.ru/api/v2/contacts";
    private $contactConverter;


    public function __construct(OAuth2ClientInterface $oAuth2Client, ContactConverter $contactConverter)
    {
        parent::__construct($oAuth2Client);
        $this->contactConverter = $contactConverter;
    }


    /**
     * Find and return Contact entity by email
     * @param String $email
     * @return Contact | null
     * @throws Exception - thrown when response code isn't successful
     */
    public function getContactByEmail(String $email): ?Contact
    {
        $contactsData = $this->getContactsByQuery($email);
        $contactData = $this->findFirstContactDataByEmail($contactsData, $email);
        return $contactData !== null ? $this->contactConverter->convertToEntity($contactData) : null;
    }


    /**
     * Find and return Contact entity by phone
     * @param String $phone
     * @return Contact | null
     * @throws Exception - thrown when response code isn't successful
     */
    public function getContactByPhone(String $phone): ?Contact
    {
        $contacts = $this->getContactsByQuery($phone);
        $contactData = $this->findFirstContactDataByPhone($contacts, $phone);
        return $contactData !== null ? $this->contactConverter->convertToEntity($contactData) : null;
    }


    /**
     * Add new contact add return the same contact with filled id
     * @param Contact $contact to add
     * @return Contact with id
     * @throws Exception - thrown when response code isn't successful
     */
    public function addContact(Contact $contact): Contact
    {
        $data = array('add' => array($this->contactConverter->convertToArray($contact)));
        $curl = $this->prepareCurlClient($this->link, 'POST', $data);
        $out = curl_exec($curl);
        $code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);

        $this->checkResponseCode((int)$code);

        $response = json_decode($out, true);
        $id = $response['_embedded']['items'][0]['id'];
        if(isset($id)) {
            $contact->setId($id);
            return $contact;
        } else {
            throw new Exception("Something went wrong while adding new contact {$contact->getName()}");
        }
    }


    /**
     * Update existing contact
     * @param Contact $contact
     * @throws Exception - thrown when response code isn't successful
     */
    public function updateContact(Contact $contact): void
    {
        $data = array('update' => array($this->contactConverter->convertToArray($contact)));
        $curl = $this->prepareCurlClient($this->link, 'POST', $data);
        curl_exec($curl);
        $code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);

        $this->checkResponseCode((int)$code);
    }


    /**
     * Find contacts by given custom field
     * @param String $query - custom field to find
     * @return array - array of contacts
     * @throws Exception - thrown when response code isn't successful
     */
    private function getContactsByQuery(String $query): array
    {
        $link = $this->link . "?query=" . $query;

        $curl = $this->prepareCurlClient($link, 'GET');
        $out = curl_exec($curl);
        $code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);

        $this->checkResponseCode((int)$code);

        $response = json_decode($out, true);
        $contacts = $response['_embedded']['items'];

        return $contacts !== null ? $contacts : [];
    }


    /**
     * Find and return first matching contact by email from given array of contacts
     * @param array $contactsData - array of contacts data
     * @param String $email - email of contact to find
     * @return array contact | null
     */
    private function findFirstContactDataByEmail(array $contactsData, String $email): ?array
    {
        forEach ($contactsData as $contactData) {
            forEach ($contactData['custom_fields'] as $custom_field) {
                if ($custom_field['code'] === 'EMAIL') {
                    forEach ($custom_field['values'] as $value) {
                        if ($value['value'] === $email) {
                            return $contactData;
                        }
                    }
                }
            }
        }
        return null;
    }


    /**
     * Find and return first matching contact by phone from given array of contacts
     * @param array $contactsData - array contacts data
     * @param String $phone - phone of contact to find
     * @return array contact | null
     */
    private function findFirstContactDataByPhone(array $contactsData, String $phone): ?array
    {
        forEach ($contactsData as $contactData) {
            forEach ($contactData['custom_fields'] as $custom_field) {
                if ($custom_field['code'] === 'PHONE') {
                    forEach ($custom_field['values'] as $value) {
                        if ($value['value'] === $phone) {
                            return $contactData;
                        }
                    }
                }
            }
        }
        return null;
    }
}