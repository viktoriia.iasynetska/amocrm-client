<?php
namespace clients;

use entities\Contact;

interface ContactClientInterface
{
    public function addContact(Contact $contact): Contact;
    public function updateContact(Contact $contact): void;
    public function getContactByEmail(String $email): ?Contact;
    public function getContactByPhone(String $phone): ?Contact;
}