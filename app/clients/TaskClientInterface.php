<?php
namespace clients;

use entities\Task;

interface TaskClientInterface
{
    public function addTask(Task $task): Task;
}