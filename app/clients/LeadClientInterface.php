<?php
namespace clients;

use entities\Lead;

interface LeadClientInterface
{
    public function addLead(Lead $lead): Lead;
    public function getLeadsForCurrentDay(): array;
}