<?php
namespace clients;

use oauth2\OAuth2ClientInterface;
use entities\converters\LeadConverter;
use entities\Lead;
use \Exception;


class ApiLeadClient extends AbstractApiClient implements LeadClientInterface
{
    private $link = "https://yasinetv.amocrm.ru/api/v2/leads";
    private $leadConverter;


    public function __construct(OAuth2ClientInterface $oAuth2Client, LeadConverter $leadConverter)
    {
        parent::__construct($oAuth2Client);
        $this->leadConverter = $leadConverter;
    }


    /**
     * Add new Lead
     * @param Lead $lead - new Lead to add
     * @return Lead with filled id
     * @throws Exception - thrown when response code isn't successful
     */
    public function addLead(Lead $lead): Lead
    {
        $data = array('add' => array($this->leadConverter->convertToArray($lead)));
        $curl = $this->prepareCurlClient($this->link, 'POST', $data);
        $out = curl_exec($curl);
        $code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);

        $this->checkResponseCode((int)$code);

        $response = json_decode($out, true);
        $id = $response['_embedded']['items'][0]['id'];
        if (isset($id)) {
            $lead->setId($id);
            return $lead;
        } else {
            throw new Exception("Something went wrong while adding new lead {$lead->getName()}");
        }
    }


    /**
     * Get list of all Leads for current day
     * @return array of Leads
     * @throws Exception - thrown when response code isn't successful
     */
    public function getLeadsForCurrentDay(): array
    {
        $today = date("d.m.Y");
        $tomorrow = date("d.m.Y", mktime(0, 0, 0, date("d"), date("m") + 1, date("Y")));
        $link = $this->link . "?filter[date_create][from]=$today&filter[date_create][to]=$tomorrow";

        $curl = $this->prepareCurlClient($link, 'GET');
        $out = curl_exec($curl);
        $code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);

        $this->checkResponseCode((int)$code);

        $response = json_decode($out, true);
        $leads = $response['_embedded']['items'];
        $leads = isset($leads) ? $leads : [];

        return array_map(function ($lead) {
            return $this->leadConverter->convertToEntity($lead);
        }, $leads);
    }
}