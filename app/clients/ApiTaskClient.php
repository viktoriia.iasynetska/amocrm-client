<?php
namespace clients;

use oauth2\OAuth2ClientInterface;
use entities\converters\TaskConverter;
use entities\Task;
use \Exception;


class ApiTaskClient extends AbstractApiClient implements TaskClientInterface
{
    private $link = "https://yasinetv.amocrm.ru/api/v2/tasks";
    private $taskConverter;


    public function __construct(OAuth2ClientInterface $oAuth2Client, TaskConverter $taskConverter)
    {
        parent::__construct($oAuth2Client);
        $this->taskConverter = $taskConverter;
    }


    /**
     * Add new Task
     * @param Task $task - new Task to add
     * @return Task with filled id
     * @throws Exception - thrown when response code isn't successful
     */
    public function addTask(Task $task): Task
    {
       $data = array('add' => array($this->taskConverter->convertToArray($task)));
       $curl = $this->prepareCurlClient($this->link, 'POST', $data);
       $out = curl_exec($curl);
       $code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
       curl_close($curl);

       $this->checkResponseCode((int)$code);

       $response = json_decode($out, true);
       $id = $response['_embedded']['items'][0]['id'];
       if (isset($id)) {
           $task->setId($id);
           return $task;
       } else {
           throw new Exception("Something went wrong while adding new Task {$task->getName()}");
       }
    }
}