<?php
namespace clients;

use Exception;
use oauth2\AmoCrmOAuth2Client;

abstract class AbstractApiClient
{
    private $oAuth2Client;

    private $errors = [
        301 => 'Moved permanently',
        400 => 'Bad request',
        401 => 'Unauthorized',
        403 => 'Forbidden',
        404 => 'Not found',
        500 => 'Internal server error',
        502 => 'Bad gateway',
        503 => 'Service unavailable',
    ];


    public function __construct(AmoCrmOAuth2Client $oAuth2Client)
    {
        $this->oAuth2Client = $oAuth2Client;
    }


    /**
     * Prepare cURL client and set main properties
     * @param String $link
     * @param String $requestType - e.g. POST, GET
     * @param array $data - will be sent as body
     * @return cURL object
     */
    protected function prepareCurlClient(String $link, String $requestType, Array $data = [])
    {
        $curl = curl_init();

        $headers = ['Authorization: Bearer ' . $this->oAuth2Client->getAccessToken()];
        if($requestType === 'POST') {
            array_push($headers, 'Content-Type:application/json');
        }

        curl_setopt($curl,CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl,CURLOPT_USERAGENT,'amoCRM-API-client/1.0');
        curl_setopt($curl,CURLOPT_URL, $link);
        curl_setopt($curl,CURLOPT_HTTPHEADER,$headers);
        curl_setopt($curl,CURLOPT_HEADER, false);
        curl_setopt($curl,CURLOPT_CUSTOMREQUEST, $requestType);
        curl_setopt($curl,CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($curl,CURLOPT_SSL_VERIFYPEER, 1);
        curl_setopt($curl,CURLOPT_SSL_VERIFYHOST, 2);

        return $curl;
    }

    /**
     * Check given response code and throw Exception if it is not successful
     * @param int $code - response code to check
     * @throws Exception - thrown when response code isn't successful
     */
    protected function checkResponseCode(int $code): void
    {
        if ($code != 200 && $code != 204) {
            throw new Exception(isset($this->errors[$code]) ? $this->errors[$code] : 'Undescribed error', $code);
        }
    }
}

