<?php
namespace clients;


interface UserClientInterface
{
    public function getUsers(): array;
}