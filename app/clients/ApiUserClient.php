<?php
namespace clients;

use oauth2\OAuth2ClientInterface;
use entities\converters\UserConverter;
use Exception;


class ApiUserClient extends AbstractApiClient implements UserClientInterface
{
    private $link = "https://yasinetv.amocrm.ru/api/v2/account";
    private $userConverter;

    public function __construct(OAuth2ClientInterface $oAuth2Client, UserConverter $userConverter)
    {
        parent::__construct($oAuth2Client);
        $this->userConverter = $userConverter;
    }


    /**
     * Get list of all User
     * @return array of Users
     * @throws Exception - thrown when response code isn't successful
     */
    public function getUsers(): array
    {
        $link = $this->link . "?with=users";

        $curl = $this->prepareCurlClient($link, 'GET');
        $out = curl_exec($curl);
        $code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);

        $this->checkResponseCode((int)$code);

        $response = json_decode($out, true);
        $users = $response['_embedded']['users'];
        $users = isset($users) ? $users : [];

        return array_map(function ($user) {
            return $this->userConverter->convertToEntity($user);
        }, $users);
    }
}