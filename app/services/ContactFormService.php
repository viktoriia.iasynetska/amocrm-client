<?php
namespace services;

use clients\ContactClientInterface;
use clients\LeadClientInterface;
use clients\TaskClientInterface;
use clients\UserClientInterface;
use entities\Contact;
use entities\ContactForm;
use entities\Lead;
use entities\Task;
use entities\User;
use Exception;

class ContactFormService
{
    private $contactClient;
    private $leadClient;
    private $taskClient;
    private $userClient;


    public function __construct(
        ContactClientInterface $contactClient,
        LeadClientInterface $leadClient,
        TaskClientInterface $taskClient,
        UserClientInterface $userClient
    )
    {
        $this->contactClient = $contactClient;
        $this->leadClient = $leadClient;
        $this->taskClient = $taskClient;
        $this->userClient = $userClient;
    }


    /**
     * Handle given contact form, creates Contact if not exist, create Lead and task
     * @param ContactForm $contactForm
     * @throws Exception - thrown when response code isn't successful
     */
    public function handleContactForm(ContactForm $contactForm)
    {
        $contact = $this->getExistingContact($contactForm);
        $responsibleId = null;

        if (isset($contact)) {
            $responsibleId = $contact->getResponsible();
            $this->contactClient->updateContact($contact);
        } else {
            $users = $this->userClient->getUsers();
            $leads = $this->leadClient->getLeadsForCurrentDay();
            $responsibleId = $this->appointResponsible($users, $leads)->getId();
            $contact = new Contact($contactForm->getName(), $contactForm->getPhone(), $contactForm->getEmail(), $responsibleId);
            $this->contactClient->addContact($contact);
        }

        $lead = new Lead('Заявка с сайта', $contact->getId(), $responsibleId);
        $this->leadClient->addLead($lead);
        $task = new Task('Перезвонить клиенту', $contact->getResponsible(), $lead->getId(), time() + 86400);
        $this->taskClient->addTask($task);
    }


    private function getExistingContact(ContactForm $contactForm)
    {
        $contact = $this->contactClient->getContactByEmail($contactForm->getEmail());
        if (!isset($contact)) {
            $contact = $this->contactClient->getContactByPhone($contactForm->getPhone());
        }
        return $contact;
    }

    /**
     * Appoint responsible user with smallest number of Leads for one day
     * @param array $users
     * @param array $leads
     * @return User with smallest number of Leads
     */
    private function appointResponsible(array $users, array $leads): User
    {
        // Filter out administrators and non-active users
        $users = array_filter($users, function ($user) {
            return !$user->isAdmin() && $user->isActive();
        });

        // Create data structure to store users id as keys, counter and contacts id
        $numOfLeadsByUsers = array_map(function () {
            return array(
                'counter' => 0,
                'contacts' => array()
            );
        }, $users);

        // Calculate number of leads for users and store them to the data structure
        foreach ($leads as $lead) {
            // Check if the lead is not assigned to admin
            if (isset($users[$lead->getResponsible()])) {
                // Check if contact in the lead is already counted
                $isContactCounted = array_key_exists($lead->getContact(), $numOfLeadsByUsers[$lead->getResponsible()]['contacts']);
                if (!$isContactCounted) {
                    $numOfLeadsByUsers[$lead->getResponsible()]['counter'] += 1;
                    $numOfLeadsByUsers[$lead->getResponsible()]['contacts'][$lead->getContact()] = true;
                }
            }
        }

        // Sort data structure by number of leads, first user with smallest number
        uasort($numOfLeadsByUsers, function ($user1, $user2) {
            if ($user1['counter'] < $user2['counter']) {
                return -1;
            }
            if ($user1['counter'] > $user2['counter']) {
                return 1;
            }
            return 0;
        });

        // Get first user id with smallest number of leads
        reset($numOfLeadsByUsers);
        $userId = key($numOfLeadsByUsers);

        return $users[$userId];
    }
}