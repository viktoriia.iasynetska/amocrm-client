<?php
namespace services\email;

use entities\ContactForm;
use PHPMailer\PHPMailer\PHPMailer;
use Exception;

class EmailService
{
    private $smtpHost = 'ssl://smtp.yandex.ru';
    private $smtpPort = 465;
    private $smtpUser = 'yasinetv@yandex.ru';
    private $smtpPassword = 'Qwerty123';

    private $to = 'yasinetv@yandex.ru';
    private $subject = 'Заявка с сайта';
    private $from = 'yasinetv@yandex.ru';

    public function sendEmail(ContactForm $contactForm): void
    {
        $mailer = new PHPMailer();

        $mailer->isSMTP();
        $mailer->SMTPAuth = true;
        $mailer->Host = $this->smtpHost;
        $mailer->Port = $this->smtpPort;
        $mailer->Username = $this->smtpUser;
        $mailer->Password = $this->smtpPassword;

        $mailer->From = $this->from;
        $mailer->addAddress($this->to);
        $mailer->Subject = $this->subject;
        $mailer->isHTML(true);
        $mailer->CharSet = 'UTF-8';
        $mailer->Body = $this->getMessage($contactForm);

        if (!$mailer->send()) {
            throw new Exception ('Error while sending email: ' . $mailer->ErrorInfo);
        }
    }


    private function getMessage(ContactForm $contactForm): String
    {
        $template = file_get_contents( __DIR__ . DIRECTORY_SEPARATOR . 'email.tpl');
        $template = str_replace('%title%', $this->subject, $template);
        $template = str_replace('%name%', $contactForm->getName(), $template);
        $template = str_replace('%phone%', $contactForm->getPhone(), $template);
        $template = str_replace('%email%', $contactForm->getEmail(), $template);

        return $template;
    }
}