<?php


namespace entities;


class User
{
    private $id;
    private $isAdmin;
    private $isActive;


    public function __construct(String $id, bool $isAdmin, bool $isActive)
    {
        $this->id = $id;
        $this->isAdmin = $isAdmin;
        $this->isActive = $isActive;
    }


    public function getId(): String
    {
        return $this->id;
    }


    public function isAdmin(): bool
    {
        return $this->isAdmin;
    }


    public function isActive(): bool
    {
        return $this->isActive;
    }
}
