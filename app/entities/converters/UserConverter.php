<?php
namespace entities\converters;

use entities\User;

class UserConverter
{
    /**
     * Create User entity from given array
     * @param $userData - associative array lead data
     * @return User - User object
     */
    public function convertToEntity(array $userData): User
    {
        return new User(
            $userData['id'],
            $userData['is_admin'],
            $userData['is_active']
        );
    }
}