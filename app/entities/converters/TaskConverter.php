<?php
namespace entities\converters;

use entities\Task;

class TaskConverter
{
    /**
     * Create array from given Task
     * @param $task - Task object
     * @return array
     */
    public function convertToArray(Task $task): array
    {		
        return array(
			'text' => $task->getText(),
            'responsible_user_id' => $task->getResponsibleUserId(),
			'element_id' => $task->getElementId(),
			'complete_till_at' => $task->getCompleteTill(),
			'id' => $task->getId(),
			'element_type' => $task->getElementType(),
			'task_type' => $task->getTaskType()
        );
    }
}