<?php
namespace entities\converters;

use entities\Contact;

class ContactConverter
{
    /**
     * Create array from given Contact
     * @param $contact - Contact object
     * @return array
     */
    public function convertToArray(Contact $contact): array
    {
        return array(
            'id' => $contact->getId(),
            'name' => $contact->getName(),
            'updated_at' => time(),
            'responsible_user_id' => $contact->getResponsible(),
            'custom_fields' => array(
                array(
                    'code' => 'PHONE',
                    'values' => array(
                        array(
                            'value' => $contact->getPhone(),
                            'enum' => "OTHER",
                        )
                    ),
                ),
                array(
                    'code' => 'EMAIL',
                    'values' => array(
                        array(
                            'value' => $contact->getEmail(),
                            'enum' => "OTHER",
                        )
                    ),
                )
            )
        );
    }


    /**
     * Create Contact entity from given array
     * @param $contactData - associative array contact data
     * @return Contact - Contact object
     */
    public function convertToEntity(array $contactData): Contact
    {
        $phone = '';
        $email = '';

        forEach ($contactData['custom_fields'] as $custom_field) {
            if ($custom_field['code'] === 'PHONE') {
                forEach ($custom_field['values'] as $value) {
                    $phone = $value['value'];
                }
            }
            if ($custom_field['code'] === 'EMAIL') {
                forEach ($custom_field['values'] as $value) {
                    $email = $value['value'];
                }
            }
        }

        return new Contact(
            $contactData['name'],
            $phone,
            $email,
            $contactData['responsible_user_id'],
            $contactData['id']
        );
    }
}