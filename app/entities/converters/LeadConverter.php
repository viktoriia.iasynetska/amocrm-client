<?php
namespace entities\converters;

use entities\Lead;

class LeadConverter
{
    /**
     * Create array from given Lead
     * @param $lead - Lead object
     * @return array
     */
    public function convertToArray(Lead $lead): array
    {
        return array(
            'id' => $lead->getId(),
            'name' => $lead->getName(),
            'responsible_user_id' => $lead->getResponsible(),
            'contacts_id' => array(
                $lead->getContact()
            )
        );
    }


    /**
     * Create Lead entity from given array
     * @param $leadData - associative array lead data
     * @return Lead - Lead object
     */
    public function convertToEntity(array $leadData): Lead
    {
        return new Lead(
            $leadData['name'],
            $leadData['main_contact']['id'],
            $leadData['responsible_user_id'],
            $leadData['id']
        );
    }
}