<?php
namespace entities;


class ContactForm
{
    private $phone;
    private $email;
    private $name;


    public function __construct(String $phone, String $email, String $name = null)
    {
        $this->phone = $phone;
        $this->email = $email;
        $this->name = $name;
    }


    public function getPhone(): String
    {
        return $this->phone;
    }


    public function getEmail(): String
    {
        return $this->email;
    }


    public function getName(): String
    {
        return $this->name;
    }
}