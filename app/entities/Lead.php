<?php


namespace entities;


class Lead
{
    private $name;
    private $contact;
    private $responsible;
    private $id;


    public function __construct(String $name, String $contact, String $responsible, String $id = null)
    {
        $this->name = $name;
        $this->contact = $contact;
        $this->responsible = $responsible;
        $this->id = $id;
    }


    public function getName(): String
    {
        return $this->name;
    }


    public function setName(String $name): void
    {
        $this->name = $name;
    }


    public function getContact(): String
    {
        return $this->contact;
    }


    public function setContact(String $contact): void
    {
        $this->contact = $contact;
    }


    public function getResponsible(): String
    {
        return $this->responsible;
    }


    public function setResponsible(String $responsible): void
    {
        $this->responsible = $responsible;
    }


    public function getId(): ?String
    {
        return $this->id;
    }


    public function setId($id): void
    {
        $this->id = $id;
    }
}