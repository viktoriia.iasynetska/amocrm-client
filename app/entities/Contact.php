<?php
namespace entities;


class Contact
{
    private $name;
    private $phone;
    private $email;
    private $responsible;
    private $id;


    public function __construct(
        String $name,
        String $phone,
        String $email,
        String $responsible,
        String $id = null
    ){
        $this->name = $name;
        $this->phone = $phone;
        $this->email = $email;
        $this->responsible = $responsible;
        $this->id = $id;
    }


    public function getName(): String
    {
        return $this->name;
    }


    public function setName(String $name): void
    {
        $this->name = $name;
    }


    public function getPhone(): String
    {
        return $this->phone;
    }


    public function setPhone(String $phone): void
    {
        $this->phone = $phone;
    }


    public function getEmail(): String
    {
        return $this->email;
    }


    public function setEmail(String $email): void
    {
        $this->email = $email;
    }


    public function getResponsible(): String
    {
        return $this->responsible;
    }


    public function setResponsible(String $responsible): void
    {
        $this->responsible = $responsible;
    }


    public function getId(): ?String
    {
        return $this->id;
    }


    public function setId(String $id): void
    {
        $this->id = $id;
    }
}