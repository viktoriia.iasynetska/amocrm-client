<?php


namespace entities;


class Task
{
    private $text;
    private $responsibleUserId;
	private $elementId;
	private $completeTill;
	private $id;
	
	private $elementType;
	private $taskType;


    public function __construct(String $text, String $responsibleUserId, String $elementId, int $completeTill, String $id = null)
    {
        $this->text = $text;
        $this->responsibleUserId = $responsibleUserId;
        $this->elementId = $elementId;
        $this->completeTill = $completeTill;
        $this->id = $id;

		// Set default values
        $this->elementType = 2;
        $this->taskType = 1;
    }


    public function getText(): String
    {
        return $this->text;
    }


    public function setText(String $text): void
    {
        $this->text = $text;
    }


    public function getResponsibleUserId(): String
    {
        return $this->responsibleUserId;
    }


    public function setResponsibleUserId(String $responsibleUserId): void
    {
        $this->responsibleUserId = $responsibleUserId;
    }


    public function getElementId(): String
    {
        return $this->elementId;
    }


    public function setElementId(String $elementId): void
    {
        $this->elementId = $elementId;
    }


    public function getCompleteTill(): int
    {
        return $this->completeTill;
    }


    public function setCompleteTill(int $completeTill): void
    {
        $this->completeTill = $completeTill;
    }


    public function getId(): ?String
    {
        return $this->id;
    }


    public function setId(String $id): void
    {
        $this->id = $id;
    }


    public function getElementType(): int
    {
        return $this->elementType;
    }


    public function setElementType(int $elementType): void
    {
        $this->elementType = $elementType;
    }


    public function getTaskType(): int
    {
        return $this->taskType;
    }


    public function setTaskType(int $taskType): void
    {
        $this->taskType = $taskType;
    }
}