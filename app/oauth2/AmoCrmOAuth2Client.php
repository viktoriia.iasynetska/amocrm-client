<?php

namespace oauth2;

use \Exception;

class AmoCrmOAuth2Client implements OAuth2ClientInterface
{
    private $link = 'https://yasinetv.amocrm.ru/oauth2/access_token';
    private $propertiesPath = __DIR__ . DIRECTORY_SEPARATOR . 'oauth2.properties.json';
    private $auth2Properties;

    protected $errors = [
        400 => 'Bad request',
        401 => 'Unauthorized',
        403 => 'Forbidden',
        404 => 'Not found',
        500 => 'Internal server error',
        502 => 'Bad gateway',
        503 => 'Service unavailable',
    ];


    public function __construct()
    {
        $this->auth2Properties = new OAuth2Properties($this->propertiesPath);
    }


    /**
     * Return actual and valid access token
     * @return String access token
     */
    public function getAccessToken(): String
    {
        $now = time();
        $accessTokenExpires = $this->auth2Properties->getProperty(OAuth2Properties::ACCESS_TOKEN_EXPIRES);
        $returnTokenExpires = $this->auth2Properties->getProperty(OAuth2Properties::REFRESH_TOKEN_EXPIRES);

        if ($now < $accessTokenExpires) {
            return $this->auth2Properties->getProperty(OAuth2Properties::ACCESS_TOKEN);
        } else if ($now < $returnTokenExpires) {
            return $this->getAccessTokenByRefreshToken();
        } else {
            return $this->getAccessTokenByAuthorizationCode();
        }
    }


    private function getAccessTokenByRefreshToken()
    {
        $data = [
            'client_id' => $this->auth2Properties->getProperty(OAuth2Properties::INTEGRATION_ID),
            'client_secret' => $this->auth2Properties->getProperty(OAuth2Properties::SECRET_KEY),
            'grant_type' => 'refresh_token',
            'refresh_token' => $this->auth2Properties->getProperty(OAuth2Properties::REFRESH_TOKEN),
            'redirect_uri' => $this->auth2Properties->getProperty(OAuth2Properties::REDIRECT_URI)
        ];

        $curl = $this->prepareCurlClient($data);

        $this->executeRequestAndUpdateOauth2Properties($curl);

        return $this->auth2Properties->getProperty(OAuth2Properties::ACCESS_TOKEN);
    }


    private function getAccessTokenByAuthorizationCode()
    {
        $data = [
            'client_id' => $this->auth2Properties->getProperty(OAuth2Properties::INTEGRATION_ID),
            'client_secret' => $this->auth2Properties->getProperty(OAuth2Properties::SECRET_KEY),
            'grant_type' => 'authorization_code',
            'code' => $this->auth2Properties->getProperty(OAuth2Properties::AUTHORIZATION_CODE),
            'redirect_uri' => $this->auth2Properties->getProperty(OAuth2Properties::REDIRECT_URI)
        ];

        $curl = $this->prepareCurlClient($data);

        $this->executeRequestAndUpdateOauth2Properties($curl);

        return $this->auth2Properties->getProperty(OAuth2Properties::ACCESS_TOKEN);
    }


    /**
     * Prepare cURL client and set main properties
     * @param array $data - will be sent as body
     * @return cURL object
     */
    private function prepareCurlClient(Array $data)
    {
        $curl = curl_init();

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_USERAGENT, 'amoCRM-oAuth-client/1.0');
        curl_setopt($curl, CURLOPT_URL, $this->link);
        curl_setopt($curl, CURLOPT_HTTPHEADER, ['Content-Type:application/json']);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 1);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 2);

        return $curl;
    }


    /**
     * Execute given cURL, parse result and set tokens with expires times to $this->auth2Properties
     * @param $curl - cURL object
     * @throws Exception - thrown when response code isn't successful
     */
    private function executeRequestAndUpdateOauth2Properties($curl)
    {
        $now = time();
        $out = curl_exec($curl);
        $code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);

        $code = (int)$code;
        if ($code < 200 && $code > 204) {
            throw new Exception(isset($this->errors[$code]) ? $this->errors[$code] : 'Undefined error', $code);
        }

        $response = json_decode($out, true);
        $threeMonths = (60 * 60) * 24 * 90;

        $this->auth2Properties->setProperty(OAuth2Properties::ACCESS_TOKEN, $response['access_token']);
        $this->auth2Properties->setProperty(OAuth2Properties::ACCESS_TOKEN_EXPIRES, $now + $response['expires_in']);
        $this->auth2Properties->setProperty(OAuth2Properties::REFRESH_TOKEN, $response['refresh_token']);
        $this->auth2Properties->setProperty(OAuth2Properties::REFRESH_TOKEN_EXPIRES, $now + $threeMonths);
    }
}