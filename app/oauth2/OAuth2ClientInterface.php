<?php
namespace oauth2;

interface OAuth2ClientInterface {
    public function getAccessToken(): String;
}