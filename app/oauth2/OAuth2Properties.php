<?php
namespace oauth2;

use \Exception;

class OAuth2Properties
{
    const AUTHORIZATION_CODE = 'authorizationCode';
    const INTEGRATION_ID = 'integrationID';
    const SECRET_KEY = 'secretKey';
    const ACCESS_TOKEN = 'accessToken';
    const ACCESS_TOKEN_EXPIRES = 'accessTokenExpires';
    const REFRESH_TOKEN = 'refreshToken';
    const REFRESH_TOKEN_EXPIRES = 'refreshTokenExpires';
    const REDIRECT_URI = 'redirectUri';

    private $properties;
    private $path;

    public function __construct(String $path)
    {
        // read json file and decode into array
        $this->path = $path;
        $json = file_get_contents($this->path);
        $this->properties = json_decode($json, true);

        // check if json file is valid
        if(!isset($this->properties)) {
            throw new Exception("JSON content isn't valid or absent");
        }
    }

    public function getProperty(String $key) {
        return $this->properties[$key];
    }


    /**
     * Set property and write to json file
     * @param String $key
     * @param $value
     */
    public function setProperty(String $key, $value) {
        $this->properties[$key] = $value;
        $json = json_encode($this->properties, JSON_PRETTY_PRINT);
        file_put_contents($this->path, $json);
    }

}