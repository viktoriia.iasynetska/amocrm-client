<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">

        <title><?=$title?></title>

        <!--Adding Fonts-->
        <link rel="stylesheet" type="text/css" href="css/fonts.css"/>

        <!--Custom styles for this template-->
        <link rel="stylesheet" type="text/css" href="css/style.css"/>
    </head>
    <body>
        <?=$content?>
        <script src="<?=$jsFile?>"></script>
    </body>
</html>