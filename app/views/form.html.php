<form id="form-contact" class="form" action="" method="post" onsubmit="return validateForm(this.id)">
    <div class="form-block">
        <label for="name" class="form-block__label"><?=$name?>:</label>
        <input id="name" class="form-block__field" type="text" name="name" oninput="deleteBorderStyle(this)" />
        <div class='form__error'></div>
    </div>

    <div class="form-block">
        <label for="phone" class="form-block__label field_required"><?=$phone?>:</label>
        <input id="phone" class="form-block__field required" type="tel" name="phone" oninput="deleteBorderStyle(this)" />
        <div class='form__error'></div>
    </div>

    <div class="form-block">
        <label for="email" class="form-block__label field_required"><?=$email?>:</label>
        <input id="email" class="form-block__field required" type="email" name="email" oninput="deleteBorderStyle(this)" />
        <div class='form__error'></div>
    </div>

    <span class="form__note">&#42;</span><small> - <?=$required_fields?></small>

    <input id="subBtn" class="form__btn" type="submit" value="<?=$send?>" />
</form>

