const errorMessages = {
    err_empty: "The field is required!",
    err_letters: "The field may only consist of letters!",
    err_num: "The field may only consist of numbers!",
    err_email: "Provide a valid email address!",
    err_length_10to12:"The field must have between 10 and 12 characters!"
};

/**
 * Checking form and clean errorMessages.
 * @param formId - id of submitted form.
 * @returns Result of validation. true - valid,
 * 								  false - not valid.
 */
function validateForm(formId)
{
    const form = document.getElementById(formId);

    const errorDivs = form.getElementsByClassName("form__error");
    cleanErrorDivs(errorDivs);

    return validateInputFields(form);
}


/**
 * Checking fields of form.
 * @param form - submitted form.
 * @returns Result of validation. true - valid,
 * 								  false - not valid.
 */
function validateInputFields(form)
{
    let valid = true;
    const formElements = form.elements;

    for(let i=0; i<formElements.length; i++)
    {
        let element = formElements.item(i);
        element.value = element.value.trim();

        if(element.value === "")
        {
            if (element.classList.contains("required"))
            {
                addRedBorderStyle(element);
                addErrorMessage(element, "err_empty");
                valid = false;
            }
        }
        else
        {
            switch(element.id)
            {
                case "name":
                    if(!isOnlyLetters(element.value))
                    {
                        addRedBorderStyle(element);
                        addErrorMessage(element, "err_letters");
                        valid = false;
                    }
                    break;

                case "phone":
                    if(!isOnlyNumbers(element.value))
                    {
                        addRedBorderStyle(element);
                        addErrorMessage(element, "err_num");
                        valid = false;
                    }
                    else if(!isLengthMatch(element.value, 10, 12))
                    {
                        addRedBorderStyle(element);
                        addErrorMessage(element, "err_length_10to12");
                        valid = false;
                    }
                    break;

                case "email":
                    if(!isEmailFormatCorrectly(element.value))
                    {
                        addRedBorderStyle(element);
                        addErrorMessage(element, "err_email",);
                        valid = false;
                    }
                    break;
            }
        }
    }
    return valid;
}


/**
 * Clean innerHTML of given elements.
 * @param elements - array with elements.
 **/
function cleanErrorDivs(elements)
{
    for (let element of elements) {
        element.innerHTML = '';
    }
}

/**
 * Adding border style.
 * @param element - HTML element.
 **/
function addRedBorderStyle(element)
{
    element.style.border = "1px solid red";
}


/**
 * Deleting border style.
 * @param element - HTML element.
 **/
function deleteBorderStyle(element)
{
    element.style.border = null;
}


/**
 * Checking length of text.
 * @param text - string for checking.
 * @param min - minimum allowable length.
 * @param max - maximum allowable length.
 * @returns result of checking. true - length of text is allowed,
 * 								false - length of text not allowed.
 **/
function isLengthMatch(text, min, max)
{
    return text.length >= min && text.length <= max;
}


/**
 * Checking text for letters only.
 * @param text - string for checking.
 * @returns result of checking. true - text contains only letters,
 * 								false - text contains not allowed symbols.
 **/
function isOnlyLetters(text)
{
    const textFormat = /^[A-Za-zА-Яа-я\s]+$/g;

    return textFormat.test(text);
}


/**
 * Checking text for numbers only.
 * @param text - string for checking.
 * @returns result of checking. true - text contains only numbers,
 * 								false - text contains not allowed symbols.
 **/
function isOnlyNumbers(text)
{
    const textFormat = /^\d+$/g;

    return textFormat.test(text);
}


/**
 * Checking email format.
 * @param email - string for checking.
 * @returns result of checking. true - email address is correct,
 * 								false - email address isn't correct.
 **/
function isEmailFormatCorrectly(email)
{
    const emailFormat = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;

    return emailFormat.test(email);
}


/**
 * Adding error message to submitted form
 * @param element - checked element.
 * @param errorName - name of the error found.
 **/
function addErrorMessage(element, errorName)
{
    element.nextElementSibling.innerHTML = errorMessages[errorName];
}